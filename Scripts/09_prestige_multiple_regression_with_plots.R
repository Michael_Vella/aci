#install.packages("car")

library(car)

d <- Prestige

d <-d[,c(1:4)]

plot(d,main="Matrix Scatterplot")

education_c <- scale(d$education, center = TRUE, scale = FALSE)

prestige_C <- scale(d$prestige,center = TRUE, scale = FALSE)

women_c  <- scale(d$women,center = TRUE, scale = FALSE)

d <- cbind(d,education_c,prestige_C,women_c)

model1 <- lm(income~education_c+prestige_C+women_c,data =d )

summary( model1 )

#install.packages("corplot")

library(corrplot)

d_cor <- cor(d[1:4])

corrplot(d_cor,method = "number")

model2 <- lm(income~prestige_C+women_c,data = d)
summary(model2)

install.packages("scatterplot3d")
install.packages("visreg")
install.packages("rgl")

library(scatterplot3d)
library(visreg)
library(rgl)

summary(d)

d_grid <- expand.grid(prestige_C = seq(-35,45,by=5), women_c = seq(-25,70,by=5))
d_grid$pi <- predict(model2,newdata = d_grid)

with(d, plot3d(prestige_C,women_c,income,col="blue",size=1
               ,type="s",main="3D Model"))

with(d_grid,surface3d(unique(prestige_C),unique(women_c),pi,alpha = 0.3
                     ,front ="line",back="line"))

model3 <- lm(log(income)~prestige_C +I(prestige_C^2)+women_c+I(women_c ^2),data = d)
summary(model3)

d_grid$pi <- predict(model3,newdata = d_grid)
with(d, plot3d(prestige_C,women_c,log(income),col="blue",size=1
               ,type="s",main="3D Model"))

with(d_grid,surface3d(unique(prestige_C),unique(women_c),pi,alpha = 0.3
                      ,front ="line",back="line"))