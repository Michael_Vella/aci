library("lpSolveAPI")

#create linear problem with 2 variables
lproc <-make.lp(0,2)

#set objective
lp.control(lproc, sense = "max")

#set constraints on the variables
set.objfn(lproc,c(143,60))

#set constraints
add.constraint(lproc, c(120,210), "<=", 15000)
add.constraint(lproc, c(110,30), "<=", 4000)
add.constraint(lproc, c(1,1), "<=", 75)

#display function detials
lproc

#solve 00
solve(lproc)

#display max value
get.objective(lproc)

#display values of variables 
get.variables(lproc)